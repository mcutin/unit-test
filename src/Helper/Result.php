<?php

namespace App\Helper;


use App\Service\Calculator;

class Result
{
    /**
     * @var Calculator
     */
    private $calculator;

    /**
     * Result constructor.
     * @param Calculator $calculator
     */
    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    public function getResult(int $a, int $b): int
    {
        return $this->calculator->add($a, $b);
    }

}