<?php

namespace App\Service;


class Calculator
{

    /**
     * @param integer $a
     * @param integer $b
     *
     * @return int
     */
    public function add(int $a, int $b): int
    {
        return $a+$b;
    }

}