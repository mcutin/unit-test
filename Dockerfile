FROM wakeonweb/php:7.2-fpm-alpine

ARG UID
ARG GID

RUN addgroup -S dockergroup
RUN adduser -S -G dockergroup -u ${UID} dockeruser

RUN sed -i \
	-e 's/^user = www-data/user = dockeruser/' \
	-e 's/^group = www-data/group = dockergroup/' /usr/local/etc/php-fpm.d/www.conf
