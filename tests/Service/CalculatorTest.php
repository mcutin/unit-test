<?php

namespace App\Tests\Service;

use App\Service\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function testCanAdd(): void
    {
        $calculator = new Calculator();
        $result = $calculator->add(30, 12);

        $this->assertEquals(42, $result);
    }

    public function testWrongAdd(): void
    {
        $calculator = new Calculator();
        $result = $calculator->add(30, 12);

        $this->assertNotEquals(43, $result);
    }

    /**
     * @expectedException \TypeError
     */
    public function testException()
    {

        $calculator = new Calculator();
        $calculator->add('treize', 12);

    }

}