<?php

namespace App\Tests\Helper;

use App\Helper\Result;
use App\Service\Calculator;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{

    public function testTrueGetResult()
    {

        /* Creation d'un objet prophecy pour simuler le comportement de la class Calculator. */
        $calculator = $this->prophesize(Calculator::class);

        /*
         * Creation d'un mock
         * On configure la prophecy pour qu'elle renvoie 25 la prochaine que l'on appellera la methode
         * add() avec les paramètres 12 et 13.
         */
        $calculator->add(12, 13)->willReturn(25);

        /*
         * On demande à la prophecy un dummy pour répondre à l'argument de la class Result qui attend un objet
         * Calculator.
         */
        $result = new Result($calculator->reveal());
        $data = $result->getResult(12, 13);

        $this->assertEquals(25, $data);
    }

    /**
     * @expectedException \TypeError
     */
    public function testException()
    {

        $calculator = $this->prophesize(Calculator::class);

        $calculator = new Result($calculator);
        $calculator->add('treize', 12);

    }

}